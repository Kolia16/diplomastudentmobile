import React, { useState, useEffect } from "react";

import { Provider } from "react-redux";
import store from "./src/store";

import { AppLoading } from "expo";
import { loadApplication } from "./src/loadApplication";
import { MainNavigator } from "./src/navigation/MainNavigator";



export default function App() {
  const [isReady, setIsReady] = useState(false);

  // * Загрузка шрифтів
  if (!isReady) {
    return (
      <AppLoading
        startAsync={loadApplication}
        onFinish={() => setIsReady(true)}
        onError={(err) => console.log(err)}
      />
    );
  }

  return (
    <Provider store={store}>
      <MainNavigator></MainNavigator>
    </Provider>
  );
}
