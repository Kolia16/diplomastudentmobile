export const timeLesson = [
  { number: 1, hours: 8, minutes: 30, time: "08:30" },
  { number: 2, hours: 10, minutes: 0, time: "10:00" },
  { number: 3, hours: 11, minutes: 40, time: "11:40" },
  { number: 4, hours: 13, minutes: 30, time: "13:30" },
  { number: 5, hours: 15, minutes: 0, time: "15:00" },
  { number: 6, hours: 16, minutes: 30, time: "16:30" },
];

export const typeOfOccupation = [
  { title: "Лекція", value: "lecture" },
  { title: "Практика", value: "practice" },
];
