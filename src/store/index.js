import { createStore, combineReducers, applyMiddleware } from "redux";
// ! для проботи із async action
import thunk from "redux-thunk";
import { authReducer, scanReducer, replacementReducer } from "./reducers";

const rootReducer = combineReducers({
  auth: authReducer,
  scan: scanReducer,
  replacement: replacementReducer
});

export default createStore(rootReducer, applyMiddleware(thunk));
