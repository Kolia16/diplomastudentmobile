import axios from "axios";
import AsyncStorage from "@react-native-community/async-storage";

import { LOGIN_AUTH } from "../store/types";
import { addressHosting } from "../config";

export class Http {
  static async get(dispatch, url) {
    const value = await authorization();
    const headers = { Authorization: value };
    return axios.get(addressHosting + url, { headers }).then(
      (res) => res,
      (error) => {
        errorAuth(error, dispatch);
        throw error;
      }
    );
  }

  static async post(dispatch, url, data = {}) {
    const value = await authorization();
    const headers = { Authorization: value };
    return axios.post(addressHosting + url, data, { headers }).then(
      (res) => res,
      (error) => {
        errorAuth(error, dispatch);
        throw error;
      }
    );
  }

  static async patch(dispatch, url, data = {}) {
    const value = await authorization();
    const headers = { Authorization: value };
    return axios.patch(addressHosting + url, data, { headers }).then(
      (res) => res,
      (error) => {
        errorAuth(error, dispatch);
        throw error;
      }
    );
  }

  static async delete(dispatch, url) {
    const value = await authorization();
    const headers = { Authorization: value };
    return axios.delete(addressHosting + url, { headers }).then(
      (res) => res,
      (error) => {
        errorAuth(error, dispatch);
        throw error;
      }
    );
  }
}

const authorization = async () => {
  try {
    return await AsyncStorage.getItem("@authorization");
  } catch (e) {
    console.log("Authorization", e);
  }
};

const errorAuth = (error, dispatch) => {
  if (
    error.response.status == 401 ||
    error.response.status == 404 ||
    error.response.status == 403
  ) {
    dispatch({
      type: LOGIN_AUTH,
      payload: {
        error: error.response.status,
        message: error.response.data.message,
      },
    });
  } else if (error.response.status == 404) {
    console.log(error.response);
    dispatch({
      type: LOGIN_AUTH,
      payload: {
        error: 404,
        message: "Не правильний маршрут сервера!",
      },
    });
  } else if (error.response.status == 500) {
    console.log(error.response);
    dispatch({
      type: LOGIN_AUTH,
      payload: {
        error: 500,
        message: "Помилка сервера!",
      },
    });
  }
};
