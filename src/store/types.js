//auth
export const LOGIN_AUTH = "LOGIN_AUTH";
export const CLEAN_ERROR = "CLEAN_ERROR";
export const AUTH_DISABLED = "AUTH_DISABLED";

//scan
export const TO_NOTE = "TO_NOTE";
export const SCAN_LOADING = "SCAN_LOADING";
export const THROW_OFF_SCAN = "THROW_OFF_SCAN";

// replacement
export const GET_REPLACEMENTES = "GET_REPLACEMENTES"
export const LOADING_AND_DISABLED_REPLACEMENT = "LOADING_AND_DISABLED_REPLACEMENT"