import { Http } from "../http";
import { TO_NOTE, SCAN_LOADING, THROW_OFF_SCAN } from "../types";

export const toNote = (qr) => {
  return (dispatch) => {
    dispatch({
      type: SCAN_LOADING,
    });

    Http.post(dispatch, "/qr-code/to-note", { qr }).then(
      (res) => {
        dispatch({
          type: TO_NOTE,
          payload: {
            message: res.data.message,
            status: res.status,
          },
        });
      },
      (error) => {
        dispatch({
          type: TO_NOTE,
          payload: {
            message: error.response.data.message,
            status: error.response.status,
          },
        });
      }
    );
  };
};

export const throwOffScan = () => {
  return (dispatch) => {
    dispatch({
      type: THROW_OFF_SCAN,
    });
  };
};
