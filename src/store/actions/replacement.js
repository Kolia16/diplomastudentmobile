import { Http } from "../http";
import { GET_REPLACEMENTES, LOADING_AND_DISABLED_REPLACEMENT } from "../types";

export const getReplacementes = () => {
  return (dispatch) => {
    dispatch({
      type: LOADING_AND_DISABLED_REPLACEMENT,
      payload: {
        loading: true,
      },
    });

    Http.get(dispatch, "/replacement/student").then(
      (replacementes) => {
        dispatch({
          type: GET_REPLACEMENTES,
          payload: {
            replacementes: replacementes.data,
          },
        });
      },
      (error) => {
        console.log(error.message);
      }
    );
  };
};
