import { Http } from "../http";
import { LOGIN_AUTH, CLEAN_ERROR, AUTH_DISABLED } from "../types";
import AsyncStorage from "@react-native-community/async-storage";

export const loadAuthes = () => {
  return {
    type: "",
    payload: [],
  };
};

export const cleanError = () => {
  return (dispatch) => {
    dispatch({
      type: CLEAN_ERROR,
      payload: {},
    });
  };
};

export const loginAuth = (login, password) => {
  return (dispatch) => {

    dispatch({
      type: AUTH_DISABLED,
      payload: {
        disabled: true,
      },
    });

    Http.post(dispatch, "/auth/login", { login, password }).then(
      async (response) => {
        try {
          await AsyncStorage.setItem("@authorization", response.data.token);
          dispatch({
            type: LOGIN_AUTH,
            payload: {
              isAuthorization: true,
            },
          });
        } catch (e) {
          console.log(e);
        }
      },
      (error) => {
        console.log(error.message);
      }
    );

  };
};
