import { GET_REPLACEMENTES, LOADING_AND_DISABLED_REPLACEMENT } from "../types";

const initialState = {
  replacementes: [],
  loading: false,
  disabled: false,
};

export const replacementReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADING_AND_DISABLED_REPLACEMENT:
      return {
        ...state,
        loading: action.payload.loading || false,
        disabled: action.payload.disabled || false,
      };

    case GET_REPLACEMENTES:
      return {
        ...state,
        replacementes: action.payload.replacementes || [],
        loading: false,
      };

    default:
      return state;
  }
};
