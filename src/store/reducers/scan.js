import { TO_NOTE, SCAN_LOADING, THROW_OFF_SCAN } from "../types";

const initialState = {
  message: "",
  status: null,
  loading: false,
};

export const scanReducer = (state = initialState, action) => {
  switch (action.type) {
    case SCAN_LOADING:
      return {
        ...state,
        loading: true,
      };

    case THROW_OFF_SCAN:
      return {
        ...state,
        message: "",
        status: null
      };

    case TO_NOTE:
      return {
        ...state,
        message: action.payload.message,
        status: action.payload.status,
        loading: false,
      };

    default:
      return state;
  }
};
