import React from "react";
import { StyleSheet, View, TouchableOpacity } from "react-native";

import { typeOfOccupation, timeLesson } from "../data";

import { AppText } from "../components/ui";

export const CardReplacement = ({ replacement }) => {
  

  const printGroups = () => {
    let groups = ''
    replacement.groups.forEach((x) => {
      groups += `${x.name}, `;
    });
    return groups.slice(0, groups.length - 2);
  };

  const printTypeOfOccupation = () => {
    const title = typeOfOccupation.find(
      (x) => x.value === replacement.type_of_occupation
    ).title;
    return title;
  };

  const printDate = (replacementDate) => {
    const date = new Date(replacementDate);
    return `${date.getDate()}.${date.getMonth()+1}.${date.getFullYear()}`;
  };

  const printTime = () => {
    return timeLesson.find((x) => x.number === replacement.number_lessons).time;
  };


  return (
    <TouchableOpacity
      activeOpacity={0.5}
    >
      <View style={styles.container}>
        <View style={styles.firstBox}>
          <View
            style={{
              flex: 3,
            }}
          >
            <AppText>{printDate(replacement.date_create)}</AppText>
          </View>
        </View>
        <View
          style={{
            ...styles.secondBox,
            borderButtomColor: "black",
            borderBottomWidth: 1,
          }}
        >
          <View
            styel={{
              flex: 1,
            }}
          >
            <AppText style={styles.textTitle}>Предмет:</AppText>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
            }}
          >
            <AppText style={styles.text}>{replacement.subject.name}</AppText>
          </View>
        </View>
        <View
          style={{
            ...styles.secondBox,
            borderButtomColor: "black",
            borderBottomWidth: 1,
          }}
        >
          <View
            styel={{
              flex: 1,
            }}
          >
            <AppText style={styles.textTitle}>Групи:</AppText>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
            }}
          >
            <AppText style={styles.text}>{printGroups()}</AppText>
          </View>
        </View>
        <View
          style={{
            ...styles.secondBox,
            borderButtomColor: "black",
            borderBottomWidth: 1,
          }}
        >
          <View
            styel={{
              flex: 1,
            }}
          >
            <AppText style={styles.textTitle}>Тип заняття:</AppText>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
            }}
          >
            <AppText style={styles.text}>{printTypeOfOccupation()}</AppText>
          </View>
        </View>
        <View
          style={{
            ...styles.secondBox,
            borderButtomColor: "black",
            borderBottomWidth: 1,
          }}
        >
          <View
            styel={{
              flex: 1,
            }}
          >
            <AppText style={styles.textTitle}>Викладач:</AppText>
          </View>
          <View
            style={{
              flex: 1,
              justifyContent: "center",
            }}
          >
            <AppText style={styles.text}>{replacement.teacher.name}</AppText>
          </View>
        </View>
        <View style={styles.treeBox}>
          <View
            style={{
              flex: 1,
            }}
          >
            <AppText style={styles.lastBoxText}>Відміняне заняття:</AppText>
            <AppText style={styles.lastBoxText}>
              {printDate(replacement.date_cancel)}
            </AppText>
          </View>
          <View
            style={{
              flex: 1,
            }}
          >
            <AppText style={styles.lastBoxText}>Перенесене заняття:</AppText>
            <AppText style={styles.lastBoxText}>
              {printDate(replacement.date_carryover)}
            </AppText>
            <AppText style={styles.lastBoxText}>{printTime()}</AppText>
            <AppText style={styles.lastBoxText}>
              Кількість занять: {replacement.number_of_lessons}
            </AppText>
            <AppText style={styles.lastBoxText}>
              Аудиторія: {replacement.audience}
            </AppText>
          </View>
        </View>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  container: {
    margin: 15,
    padding: 10,
    backgroundColor: "#fff",
    borderTopLeftRadius: 15,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
  },
  firstBox: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginBottom: 5,
    paddingBottom: 5,
  },
  secondBox: {
    //borderButtomColor: "black",
    //borderBottomWidth: 1,
    marginBottom: 5,
    paddingBottom: 5,
    flexDirection: "row",
    justifyContent: "space-between",
  },
  treeBox: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  lastBoxText: {
    textAlign: "center",
  },
  textTitle: {
    paddingRight: 5,
    fontSize: 16,
    fontWeight: "bold",
  },
  text: {
    fontSize: 16,
  },
});
