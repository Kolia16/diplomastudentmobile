import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { QrScanScreen } from "../../screens/scan";
import { THEME } from "../../theme";

const Stack = createStackNavigator();

export const QrCodeNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="QrScanScreen"
      screenOptions={{
        headerTintColor: THEME.COLOR_4,
        headerTitleAlign: "center",
        headerStyle: { backgroundColor: THEME.COLOR_1 },
      }}
    >
      <Stack.Screen
        name="QrScanScreen"
        component={QrScanScreen}
        options={{
          title: "Сканер",
        }}
      />
    </Stack.Navigator>
  );
};
