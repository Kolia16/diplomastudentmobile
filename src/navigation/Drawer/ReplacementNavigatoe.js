import React from "react";
import { createStackNavigator } from "@react-navigation/stack";
import { ReplacementesScreen } from "../../screens/replacement";
import { THEME } from "../../theme";

const Stack = createStackNavigator();

export const ReplacementNavigatoe = () => {
  return (
    <Stack.Navigator
      initialRouteName="ReplacementesScreen"
      screenOptions={{
        headerTintColor: THEME.COLOR_4,
        headerTitleAlign: "center",
        headerStyle: { backgroundColor: THEME.COLOR_1 },
      }}
    >
      <Stack.Screen
        name="ReplacementesScreen"
        component={ReplacementesScreen}
        options={{
          title: "Заміни:",
        }}
      />
    </Stack.Navigator>
  );
};
