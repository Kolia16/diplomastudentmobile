import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { loadAuthes } from "../store/actions";
import { LoginScreen } from "../screens/LoginScreen";

import { createDrawerNavigator } from "@react-navigation/drawer";
import { NavigationContainer } from "@react-navigation/native";

import { THEME } from "../theme";
import { QrCodeNavigator, ReplacementNavigatoe } from "./Drawer";

const Drawer = createDrawerNavigator();

export const MainNavigator = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(loadAuthes());
  }, [dispatch]);

  const auth = useSelector((state) => state.auth.isAuthorization);

  if (!auth) return <LoginScreen></LoginScreen>;

  return (
    <NavigationContainer>
      <Drawer.Navigator
        initialRouteName="ReplacementNavigatoe"
        drawerStyle={{ backgroundColor: THEME.COLOR_1 }}
        drawerContentOptions={{
          activeTintColor: THEME.COLOR_4,
          inactiveTintColor: THEME.COLOR_4,
          inactiveBackgroundColor: THEME.COLOR_3,
          activeBackgroundColor: THEME.COLOR_2,
        }}
      >
        <Drawer.Screen
          name="ReplacementNavigatoe"
          component={ReplacementNavigatoe}
          options={{ title: "Заміни" }}
        />
        <Drawer.Screen
          name="QrCodeNavigator"
          component={QrCodeNavigator}
          options={{ title: "Створити QR-код" }}
        />
      </Drawer.Navigator>
    </NavigationContainer>
  );
};
