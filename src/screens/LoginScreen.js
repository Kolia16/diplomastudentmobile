import React, { useEffect, useState } from "react";
import { useSelector, useDispatch } from "react-redux";
import { StyleSheet, View, Text, Alert } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome5";
import { Input, Button } from "react-native-elements";
import { THEME } from "../theme";
import { cleanError, loginAuth,  } from "../store/actions";

export const LoginScreen = () => {
  const [login, setLogin] = useState("");
  const [password, setPassword] = useState("");

  const dispatch = useDispatch();

  const { message, error, disabled } = useSelector((state) => state.auth);

  useEffect(() => {
    if (message) {
      Alert.alert(
        //title
        "Помилка",
        //body
        message || "???",
        [{ text: "OK", onPress: () => {
          dispatch(cleanError())
        } }],
        { cancelable: false }
        //clicking out side of alert will not cancel
      );
    }
  }, [message, error]);


  let validate;

  if (login.trim() && password.trim() && password.length > 5) {
    validate = false;
  } else {
    validate = true;
  }

  return (
    <View style={styles.center}>
      <View style={{ marginBottom: 10 }}>
        <Input
          placeholder="Логін"
          leftIcon={<Icon name="users" size={24} color="black" />}
          onChangeText={setLogin}
          value={login}
          inputStyle={{
            margin: 5,
          }}
        />
      </View>

      <View style={{ marginBottom: 20 }}>
        <Input
          placeholder="Пароль"
          leftIcon={<Icon name="lock" size={24} color="black" />}
          onChangeText={(p) => setPassword(p)}
          value={password}
          inputStyle={{
            margin: 5,
          }}
        />
      </View>

      <View>
        <Button
          icon={<Icon name="door-open" size={15} color="white" />}
          title="Вхід"
          titleStyle={{
            margin: 5,
          }}
          buttonStyle={{
            padding: 10,
            backgroundColor: THEME.COLOR_INDIGO,
          }}
          onPress={() => {
            dispatch(loginAuth(login, password));
          }}
          loading= {disabled}
          disabled={disabled || validate}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  center: {
    marginHorizontal: "5%",
    marginVertical: "30%",
  },
});
