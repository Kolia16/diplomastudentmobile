import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import {
  StyleSheet,
  ActivityIndicator,
  View,
  FlatList,
  Text,
} from "react-native";

import { CardReplacement } from "../../components";
import { getReplacementes } from "../../store/actions";

import { THEME } from "../../theme";
import { AppTextBold } from "../../components/ui";

export const ReplacementesScreen = () => {
  const dispatch = useDispatch();
  const { replacementes, loading } = useSelector((state) => state.replacement);

  useEffect(() => {
    dispatch(getReplacementes());
  }, [dispatch]);

  return (
    <View style={styles.container}>
      {loading ? (
        <ActivityIndicator
          style={{ padding: 40 }}
          size="large"
          color={THEME.COLOR_INDIGO_DARKEN_3}
        />
      ) : (
        <View>
          <View>
            {replacementes.length ? (
              <FlatList
                keyExtractor={(item) => item._id}
                data={replacementes}
                renderItem={({ item }) => (
                  <CardReplacement replacement={item}></CardReplacement>
                )}
              />
            ) : (
              <View style={styles.boxMsg}>
                <AppTextBold style={styles.textMsg}>Замін не має!</AppTextBold>
              </View>
            )}
          </View>
        </View>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
  },
  boxMsg: {
    backgroundColor: "#fff",
    margin: 20,
    borderRadius: 24,
    padding: 5,
  },
  textMsg: {
    textAlign: "center",
  },
});
