import React, { useState, useEffect } from "react";
import { View, StyleSheet, Alert } from "react-native";
import { useDispatch, useSelector } from "react-redux";
import { toNote, throwOffScan } from "../../store/actions";
import { BarCodeScanner } from "expo-barcode-scanner";
import { Button } from "react-native-elements";
import { AppTextBold } from "../../components/ui";
import { THEME } from "../../theme";

export const QrScanScreen = () => {
  const [hasPermission, setHasPermission] = useState(null);
  const [scanned, setScanned] = useState(false);

  const dispatch = useDispatch();

  const { message, status, loading } = useSelector((state) => state.scan);

  useEffect(() => {
    (async () => {
      const { status } = await BarCodeScanner.requestPermissionsAsync();
      BarCodeScanner.B;
      setHasPermission(status === "granted");
    })();
  }, []);
 
  useEffect(() => {
    if (message !== "" && status !== null && status !== 500) {
      Alert.alert(
        status === 201 ? "Успішно!" : "Помилка!",
        message,
        [
          {
            text: "OK",
            onPress: () => {
              dispatch(throwOffScan());
            },
          },
        ],
        { cancelable: false }
      );
    }
  }, [message, status]);

  const handleBarCodeScanned = ({ type, data }) => {
    setScanned(true);

    dispatch(toNote(data));
  };

  if (hasPermission === null) {
    return (
      <View style={styles.boxError}>
        <AppTextBold style={styles.textError}>
          Requesting for camera permission
        </AppTextBold>
      </View>
    );
  }
  if (hasPermission === false) {
    return (
      <View style={styles.boxError}>
        <AppTextBold style={styles.textError}>No access to camera</AppTextBold>
      </View>
    );
  }

  return (
    <View
      style={{
        margin: 10,
        flex: 1,
      }}
    >
      <View
        style={{
          flex: 1,
        }}
      >
        <BarCodeScanner
          onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
          style={StyleSheet.absoluteFillObject}
        />

        {scanned && (
          <Button
            title={"Сканувати ще раз"}
            onPress={() => setScanned(false)}
            buttonStyle={{ backgroundColor: THEME.COLOR_INDIGO }}
            disabled={loading}
            loading={loading}
          />
        )}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  boxError: { flex: 1, justifyContent: "center", alignItems: "center" },
  textError: {
    color: "#f00",
  },
});
